<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;
use Excel;
use PDF;

class ProductController extends Controller
{
    public function index()
    {

        $productsCollection = Product::latest();

        if (request('search')) {
            $productsCollection = $productsCollection
                ->where('description', 'like', '%' . request('search') . '%')
                ->orWhere('title', 'like', '%' . request('search') . '%');
        }

        $products = $productsCollection->paginate(10);

        return view('backend.products.index', [
            'products' => $products
        ]);
    }

    public function create()
    {

        $tags = Tag::orderBy('name', 'asc')
            ->pluck('name', 'id')
            ->toArray();

        $categories = Category::orderBy('title', 'asc')
            ->pluck('title', 'id')
            ->toArray();

        return view('backend.products.create', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        try {

            $product = Product::create([
                'category_id' => $request->category_id,
                'title' => $request->title,
                'description' => $request->description,
                'image' => $this->uploadImage(request()->file('image'))
            ]);

            $product->tags()->attach($request->tag_id);

            return redirect()->route('products.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Product $product)
    {
        //dd($product->tags);
        return view('backend.products.show', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('backend.products.edit', [
            'product' => $product,
            'categories' => $categories
        ]);
    }

    public function update(Request $request, Product $product)
    {
        try {
            $requestData = [
                'category_id' => $request->category_id,
                'title' => $request->title,
                'description' => $request->description,
            ];

            if ($request->hasFile('image')) {
                $requestData['image'] = $this->uploadImage(request()->file('image'));
            }

            $product->update($requestData);

            return redirect()->route('products.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        try {
            $product->delete();
            return redirect()->route('products.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // Softdelete
    public function trash()
    {
        $products = Product::onlyTrashed()->get();

        return view('backend.products.trashed', [
            'products' => $products
        ]);
    }

    public function restore($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();
        return redirect()->route('products.trashed')->withMessage('Successfully Restored!');
    }

    public function delete($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->forceDelete();
        return redirect()->route('products.trashed')->withMessage('Successfully Deleted Permanently!');
    }

    public function uploadImage($file)
    {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        Image::make($file)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/products/' . $fileName);
        return $fileName;
    }

    public function export()
    {
        return Excel::download(new ProductExport, 'products.xlsx');
    }

    public function downloadPdf()
    {
        $products = Product::all();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('products.pdf');
    }
}
