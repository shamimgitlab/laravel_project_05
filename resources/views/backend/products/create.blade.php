<x-backend.layouts.master>
    <x-slot name="pageTitle">
        Add Form
    </x-slot>

    <x-slot name='breadCrumb'>
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader"> Products </x-slot>

            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
            <li class="breadcrumb-item active">Add New</li>

        </x-backend.layouts.elements.breadcrumb>
    </x-slot>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Create Product <a class="btn btn-sm btn-info" href="{{ route('products.index') }}">List</a>
        </div>
        <div class="card-body">

            <x-backend.layouts.elements.errors :errors="$errors" />

            <form action="{{ route('products.store') }}" enctype="multipart/form-data" method="post">
                @csrf

                <x-backend.form.field>
                    <select name="category_id" id="category_id" class="form-select">
                        @foreach ($categories as $key=>$value)
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <x-backend.form.error name="category_id" />
                </x-backend.form.field>

                <x-backend.form.input name="title" />

                <x-backend.form.textarea name="description" />

                <x-backend.form.input name="image" type="file" />


                @foreach ($tags as $key=>$value)
                <input type="checkbox" name="tag_id[]" value="{{ $key }}"> {{ $value }}
                @endforeach

                <br/>

                <x-backend.form.button>Save</x-backend.form.button>
            </form>
        </div>
    </div>


</x-backend.layouts.master>